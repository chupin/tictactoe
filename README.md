# TicTacToe #

An attempt at implementing a simple TicTacToe game in Haskell to learn the basics of the language & its application in the context
of a game.

### How do I get set up? ###

To get up and running simply type :
	
	$ cabal configure
	$ cabal run