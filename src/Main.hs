module Main where

-- Imports
import Data.Char
import System.IO

-- |The basic square
data Symbol = Circle | Cross

-- |Convenience
instance Show Symbol where 
    show Circle = "O"
    show Cross  = "X"

instance Enum Symbol where
    succ Circle     = Cross
    succ Cross      = Circle

    toEnum 0        = Circle
    toEnum 1        = Cross

    fromEnum Circle = 0
    fromEnum Cross  = 1

instance Eq Symbol where 
    Circle == Cross  = False
    Cross  == Circle = False
    _      == _      = True

-- |A full board
type Board  = [[Maybe Symbol]]

-- |Position
type Position = (Int, Int)

-- |Board width
boardWidth :: Int
boardWidth = 3

-- |Board height
boardHeight :: Int
boardHeight = 3

-- |Fist player
firstPlayer :: Symbol
firstPlayer = Circle

-- |Empty board
emptyBoard :: Board
emptyBoard = replicate boardHeight $ replicate boardWidth Nothing

-- |Test board
testBoard :: Board
testBoard = [
        [Just Cross,  Nothing, Nothing],
        [Just Circle, Just Cross, Nothing],
        [Nothing, Just Circle, Just Cross] 
    ]

-- |Has matching symbols
hasMatchingSymbols :: [Maybe Symbol] -> Bool
hasMatchingSymbols []           = False
hasMatchingSymbols (Nothing:_)  = False
hasMatchingSymbols [Just _]     = True
hasMatchingSymbols (x:y:[])     = ( x == y )
hasMatchingSymbols (x:y:ys)     = if ( x == y ) then hasMatchingSymbols ys else False 
                            
-- |Winning condition
hasWon :: Board -> Maybe Symbol
hasWon b = returnSymbol b (2,2) 

-- |Returns the symbol at the specified board location
returnSymbol :: Board -> Position -> Maybe Symbol
returnSymbol b (x,y) = ( !! x ) $ ( !! y ) b 

-- |Sets an element in a 1d array
setElem :: [a] -> Int -> a -> [a]
setElem []     _ _ = []
setElem (_:xs) 0 v = v : xs
setElem (x:xs) n v = x : setElem xs (n - 1) v

-- |Sets a symbol into the specified board location
setSymbol :: Board -> Position -> Symbol -> Board
setSymbol b (x,y) s = setElem b y setRow where
    setRow = setElem ( b !! y ) x (Just s) 

-- |Converts the board to a string for display
showBoard :: Board -> String
showBoard = concatMap showRow where
    showRow xs = concatMap showCell xs ++ "\n"
    showCell ( Just x ) = show x 
    showCell Nothing    = "."

-- |Converts a character to a symbol
charToSymbol :: Char -> Maybe Symbol
charToSymbol = charToSymbol' . toUpper where
    charToSymbol' 'X' = Just Cross
    charToSymbol' 'O' = Just Circle
    charToSymbol'  _  = Nothing
    
-- |Reads a symbol from the keyboard
getSymbol :: IO (Maybe Symbol)
getSymbol = fmap ( charToSymbol . ( !! 0 ) )getLine 

-- |Converts a string to a number
numberToInt :: String -> Int
numberToInt []     = 0
numberToInt (x:xs) = ( digitToInt x ) * ( 10 ^ ( length xs ) ) + numberToInt xs

-- |Prompts a line
promptLine :: String -> IO String
promptLine p = putStr p >> hFlush stdout >> getLine

-- |Prompts a number
promptNumber :: String -> IO Int
promptNumber p = fmap numberToInt ( promptLine p )

-- |Prompts the user for a position
getPosition :: IO Position
getPosition = do
    x <- promptNumber "x="
    y <- promptNumber "y="
    return ( x, y )

-- |Display the player's turn message
displayPlayersTurnMessage :: Symbol -> IO ()
displayPlayersTurnMessage Cross  = putStrLn "Cross' turn"
displayPlayersTurnMessage Circle = putStrLn "Circle's turn"

-- |Display the player's winning message
displayPlayersWinningMessage :: Symbol -> IO ()
displayPlayersWinningMessage Cross  = putStrLn "Cross wins!"
displayPlayersWinningMessage Circle = putStrLn "Circle wins!"

-- |Queries the move action 
getPlayerAction :: Symbol -> IO Position
getPlayerAction s = displayPlayersTurnMessage s >> getPosition

-- |Performs a player move from prompting for the move to updating the board
doPlayerMove :: Board -> Symbol -> IO Board
doPlayerMove b s = do
    p <- getPlayerAction s
    return $ setSymbol b p s

-- |Run a single round
doOneRound :: Board -> Symbol -> IO Board
doOneRound b s = putStrLn ( showBoard b ) >> doPlayerMove b s

-- |Game loop
doTicTacToe :: Board -> Symbol -> IO ()
doTicTacToe b s = do 
    nB <- doOneRound b s
    case hasWon nB of 
        Just wS   -> displayPlayersWinningMessage wS
        otherwise -> doTicTacToe nB (succ s)

-- |Main
main :: IO ()
main = doTicTacToe emptyBoard firstPlayer